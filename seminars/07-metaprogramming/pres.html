<!DOCTYPE html><html><head><meta charset="utf-8"></head><body><textarea id="source">
class: center, middle

# Metaprogramming

---
# Metaprogramming

- Template magic (SFINAE/concepts) (+overloads +stdlib)
- constexpr/consteval (+optimizing compilation)
- Types list, designing containers (+transparent hashing)
- RTTI (+typeid_cast in ClickHouse)
- Codegen and reflection (+ vk.com kPHP)

???
- How to read stdlib and pre C++11 code.
- How to design better APIs by restricting template code usage.
- How not to make your compiler boil.
- How to design containers with help of metaprogramming.
- How to use type information in runtime for better performance.
- How (and why) to generate C++-code.

---
# Template magic

```cpp
template <class Functor, class Container>
auto transform(Functor && functor, Container & container); // C++17

auto transform(auto && functor, auto & container); // C++20, looks like Python

/// Oops, hundreds of lines of messy template messages deep in stdlib
transform([] (std::string) { ... }, vector_of_ints);
```

```cpp
int a;
std::vector<std::vector<int>> v;
std::vector<std::vector<int>>::const_iterator it = std::find( v.begin(), v.end(), a );
```

```cpp
/usr/include/c++/4.6/bits/stl_algo.h: In function ‘_RandomAccessIterator std::__find(_RandomAcce
/usr/include/c++/4.6/bits/stl_algo.h:4403:45:   instantiated from ‘_IIter std::find(_IIter, _IIt
error_code.cpp:8:89:   instantiated from here
/usr/include/c++/4.6/bits/stl_algo.h:162:4: error: no match for ‘operator==’ in ‘__first.__gnu_c
/usr/include/c++/4.6/bits/stl_algo.h:162:4: note: candidates are:
/usr/include/c++/4.6/bits/stl_pair.h:201:5: note: template bool std::operator==(const std::pair&
/usr/include/c++/4.6/bits/stl_iterator.h:285:5: note: template bool std::operator==(const std::r
...
```

???
- Template messages may be misleading
- Production code may be complex
- We need a way to restrict access to our function (better API design)

---
# Template magic

```cpp
template <class T> auto find(std::vector<T>&, const T&);

template <class Ret, class T> // Disallows stateful lambdas
(1) std::vector<Ret> transform(Ret(*functor)(const T&), const std::vector<T> &);

template <class Ret, class T> // Runtime cost
(2) std::vector<Ret> transform(std::function<Ret(const T&)>, const std::vector<T> &);
```

```cpp
template <class T> /// Concepts, C++20
(3) auto transform(std::invocable<T> auto && functor, const std::vector<T> &);

template <class F, class T, typename = std::enable_if_t<std::is_invocable_v<F, T>>>
(4) auto transform(F && functor, const std::vector<T>&); /// SFINAE, C++17
```

???
- We can use various techniques to restrict access, but SFINAE/concepts fit best.

---
# Substitution Failure Is Not An Error

```cpp
template <class It> void process(It begin, It end);

process(vector.begin(), vector.end()); /// Want to differentiate
process(array_begin_ptr, array_begin_ptr + array_size);

// Can obtain nested type: decltype(it)::type
template <class T> struct Iter { using Type = T; T * ptr; };
```

```cpp
template <class Range> void process(Range r);

process(vector); // must have .begin() and .end()
```

???
- Modern C++ allows us to write less code (e.g. range in comparison with passing explicit begin and end).
- We can perform various optimizations depending on passed types

---
# `std::enable_if[_t]`

```cpp
template <bool Cond, class T = void>
struct enable_if {};

template <class T> // Partial specialization
struct enable_if<true, T>{ using Type = T; };

template <bool Cond, class T>
using enable_if_t = typename enable_if<Cond, T>::Type;
```

```cpp
template <class F, class T, typename = std::enable_if_t<std::is_invocable_v<F, T>>>
auto transform(F && functor, const std::vector<T>&); /// SFINAE, C++17

/// typename = std::enable_if_t<true> = void
transform([](int) {}, vector_of_ints);

/// typename = std::enable_if_t<false>. No type in enable_if_t,
/// overload is banned ("ill-formed")
transform([](std::string) {}, vector_of_ints);
```

???
- Basically, a function specialization. Trying to get a type from an empty structure
  either yields a compile error or makes the overload ill-formed

---
# `std::is_invocable[_v]`

```cpp
template <class F, class T>
struct is_invocable { constexpr bool value = false; };

template <class F, class T> /// Appeared in C++11
struct is_invocable< /*some spec */> { constexpr bool value = true; };

template <class F, class T> /// Appeared in C++14
constexpr bool is_invocable_v = is_invocable<F, T>::value;
```

```cpp
template <class F, class T, typename = std::enable_if_t<std::is_invocable_v<F, T>>>
auto transform(F && functor, const std::vector<T>&); /// SFINAE, C++17
```

---
# Conditional overloads (dispatching)

```cpp

template <typename Integer, typename = std::enable_if_t<std::is_integral<Integer>::value>>
void foo(Integer);

template <typename Floating,
    typename = std::enable_if_t<std::is_floating_point<Floating>::value>>
void foo(Floating); // error: redefinition
```

```cpp
 
template <typename Integer, std::enable_if_t<std::is_integral<Integer>::value, bool> = true>
void foo(Integer);
 
template <typename Floating,
    std::enable_if_t<std::is_floating_point<Floating>::value, bool> = true>
void foo(Floating);
```

> Default template arguments are not accounted for in function template equivalence
https://en.cppreference.com/w/cpp/language/function_template#Function_template_overloading

???
- In pre C++11 (compilers without if constexpr) we can have multiple restricted overloads, but
  their implementation is ugly.

---
# Conditional overloads (modern)

```cpp
template <class T>
void foo(T) {
    if constexpr(std::is_same_v<T, int>)
      ///
    if constexpr(std::is_same_v<T, Iter>)
      /// can use typename Iter::Type as only
      /// one branch gets evaluated
    else
        static_assert(false);
}
```

???
- Tell that an ordinary if needs to evaluate all branches.

---
# `find(Range)`

```cpp
template <class Range> void process(Range r);

process(vector); // must have .begin() and .end()
```

```cpp
template <class T, typename = void>
struct is_iterable : std::false_type {};

template <class T>
struct is_iterable<T, std::void_t<decltype(std::declval<T>().begin()),
                                  decltype(std::declval<T>().end())>>
    : std::true_type {};
```

---
# `decltype(), declval<T>()`

```cpp
int a; static_assert(std::is_same_v<int, decltype(a)>);

auto a = get_const_int(); // a = int
auto a = get_const_volatile_lvalue_int(); // a = int

// decltype(get_const_int()) == const int

decltype(auto) a = get_const_int(); // a = const int
decltype(auto) a = get_const_volatile_lvalue_int(); // a = const volatile int &
```

```cpp
// For use in unevaluated contexts. Real implementation is more complicated
template <class T> auto declval() -> T&&;

struct Default { int foo() const; };
struct NonDefault { NonDefault() = delete; int foo() const; };
 
decltype(Default().foo()) n1 = 1;
//decltype(NonDefault().foo()) n2 = n1; // error: no default constructor
decltype(declval<NonDefault>().foo()) n2 = n1;
```

???
- Decltype -- obtain a type. Two ways of getting the type -- auto (does not preserve qualifiers)
  and decltype(auto).
- Declval -- operate on T in unevaluated contexts when T may not be default constructible.

---
# `decltype(auto) + lambdas`

```cpp
// not returning reference if needed, extra copy
transform(first, last, []<class T>(T&& x) {
    return foo(std::forward<T>(x));
});

transform(first, last, []<class T>(T&& x) -> decltype(auto) {
    return foo(std::forward<T>(x));
});

// invocation for overloadable functions
invoke([](auto&& x) -> decltype(test(std::forward<decltype(x)>(x)))
{
	return test(std::forward<decltype(x)>(x));
});
```

http://open-std.org/JTC1/SC22/WG21/docs/papers/2017/p0573r0.html

???
- decltype(auto) used in lambdas return types to avoid a copy. Hard to write optimal code, see proposal

---
# `std::is_nothrow_move_constructible`

---
# `std::is_nothrow_move_constructible` (solution)

```cpp
template <class T, class ...Args>
static constexpr bool nothrow_cons =
    noexcept(T(declval<Args>()...));

template<class T>
static constexpr bool nothrow_move_cons =
    nothrow_cons<T, std::add_rvalue_reference_t<T>>;
```

---
# `std::void_t`

```cpp
template<class, class = void> struct has_member_foo
  : std::false_type { };

template<class T>
struct has_member_foo<T, std::void_t<decltype( std::declval<T&>().foo() )>>
  : std::true_type { };

template <class ...> using void_t = void;
```

---
# `find(Range)` with concepts

```cpp
template <class Range> void process(Range r);

process(vector); // must have .begin() and .end()
```

```cpp
template <class T, typename = void>
struct is_iterable : std::false_type {};

template <class T>
struct is_iterable<T, std::void_t<decltype(std::declval<T>().begin()),
                                  decltype(std::declval<T>().end())>>
    : std::true_type {};
```

```cpp
template <class T>
concept is_iterable = requires (const T& a) {
  std::begin(a);
  std::end(a);
};
```

https://en.cppreference.com/w/cpp/ranges/range

---
# `std::is_polymorphic`

```cpp
template <class T>
std::true_type detect_is_polymorphic(
    /// A polymorphic type must support dynamic_cast
    decltype(dynamic_cast<const volatile void*>(static_cast<T*>(nullptr)))
);

/// Ellipsis overload has lowest priority
template <class T> std::false_type detect_is_polymorphic(...);
 
template <class T>
using is_polymorphic_v = decltype(detect_is_polymorphic<T>(nullptr));
```

- Not portable among architectures
- Not portable among compilers (MSVC)

---
# `std::is_union` and co.

> Anything you can do with a class, you can do with a union

```cpp
// libc++
template <class _Tp> /* */ is_union
    : public __libcpp_union<typename remove_cv<_Tp>::type> {};

TYPE_TRAIT_1(__is_union, IsUnion, KEYCXX) // AST/Decl.h

bool isUnion () const { return getTagKind() == TTK_Union; } //AST/APValue.h
```

https://github.com/Quuxplusone/from-scratch/blob/master/include/scratch/bits/type-traits/compiler-magic.md

---
# `if consteval`

```cpp
constexpr double power(double b, int x)
{
    if (std::is_constant_evaluated()) { /// NOT if constexpr
        // constexpr-friendly (probably, more simple) algo that 
        // does not make your compiler boil.
    } else {
        // Runtime version
    }
}
```

```cpp
constexpr bool is_constant_evaluated() noexcept
{
    if consteval { // syntactic sugar, braces required
        return true;
    }
    else {
        return false;
    }
}
```

???
- Way to write optimal code by providing two separate implementations

---
# Type list

```cpp
type_list<type1, type2>; TYPE_LIST5(T, TYPE_LIST4); // Pre C++-11, Alexandrescu-like (Loki)
type_list<type1, type2, ...>; // C++11, variadic templates

template <class ...> struct type_list {};
```

```cpp
template <size_t I, class T, class ...Other>
struct get {
    static_assert(I <= sizeof...(Other));

    using Type = std::conditional_t<(I == 0),
      T,
      typename get<I - 1, Other...>::Type>;
};
```

https://www.boost.org/doc/libs/1_77_0/libs/mp11/doc/html/simple_cxx11_metaprogramming.html

Can use to e.g. create a matrix product type with (emulating dependent types)
+ add example

---
# Transparent hashing (aka heterogeneous lookup)

```cpp
using namespace std::string_literals; using namespace std::string_view_literals;

struct string_hash
{
    using hash_type = std::hash<std::string_view>;
    using is_transparent = void;
 
    size_t operator()(std::string_view str) const { return hash_type{}(str); }
    size_t operator()(const std::string& str) const { return hash_type{}(str); }
};

std::unordered_map<std::string, size_t> map;
std::unordered_map<std::string, size_t, string_hash, std::equal_to<>> other;

/// map.find("string view"sv);  // compile error
map.find(std::string{"string view"sv}); // temporary object
map.find("std::string"s);

other.find("string view"sv); // no temporary object
other.find("std::string"s);
```

http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2018/p0919r2.html

---
# Multi hasher

```cpp
template <class HashType, class ...Types>
struct multi_hasher {
    using is_transparent = void;
    // todo
};

using mh = multi_hasher<std::string_view, const std::string&, const char *>;

const std::string_view sv = "string view";
const std::string s = "string";
const char * l = "literal";

mh{}(sv);
mh{}(s);
mh{}(l);
```

---
# Multi hasher, solution

```cpp
template <class HashType, class Arg>
struct Overload {
    size_t operator()(Arg a) const { return std::hash<HashType>{}(a); }
};

template <class HashType, class ...Types>
struct multi_hasher : Overload<HashType, HashType>, Overload<HashType, Types>... {
    using is_transparent = void;
};

using mh = multi_hasher<std::string_view, const std::string&, const char *>;

const std::string_view sv = "string view";
const std::string s = "string";
const char * l = "literal";

mh{}(sv);
mh{}(s);
mh{}(l);
```

???
- Issue: need only distinct types in Types... (
  duplicate base classes otherwise)

---
# Lambdas in unevaluated contexts

```cpp
struct IntegralReader {};
template <class T> struct ArrayReader {};
struct DReader { DReader() = delete; };

template <class T> // Better than writing traits
using type = decltype([] {
    if constexpr(std::is_integral_v<T>)
        return IntegralReader();
    else if constexpr (std::is_array_v<T>)
        return DReader();
    else
    {
        using underlying = typename T::underlying;
        using decayed = std::decay_t<underlying>;
        return ArrayReader<decayed>();
    }
}()); //IILE

using a = type<int>;
using b = type<std::array<int, 20>>;
```

Not supported by clang.

---
# RTTI

```cpp
// Way to get type info (name, hash) in _runtime_ 
// (as opposed to decltype())
typeid(A) -> std::type_info

// Wrapper around type_info that can be used e.g. for
// indexing containers
std::type_index
```

```cpp
struct A { virtual ~A() {} };
 
struct B : A {};
struct C : A {};
 
std::unordered_map<std::type_index, std::string> type_names;

// Can later use type_names to get exact types
type_names[std::type_index(typeid(A))] = "A";
type_names[std::type_index(typeid(B))] = "B";
type_names[std::type_index(typeid(C))] = "C";
```

---
# RTTI

```cpp
// Possibly faster then dynamic_cast
template <typename To, typename From>
To * typeid_cast(From * from) {
    if ((typeid(From) == typeid(To)))
        return static_cast<To *>(from);
    else
        return nullptr;
}

std::unique_ptr<Base> up = std:make_unique<Derived>();
// Later in runtime
Derived * ptr = typeid_cast<Derived>(up.get());
```

https://github.com/ClickHouse/ClickHouse/blob/master/src/Common/typeid_cast.h


---
# Codegen & Reflection

- vk.com kPHP -- translating a subset of PHP into C++ (Legacy)
- Generating entities definitions from schema (one-to-many)
- Filtering container by predefined states (Less boilerplate)
- Simplifying generic code.

https://habr.com/ru/company/vk/blog/527420

https://en.cppreference.com/w/cpp/keyword/reflexpr

---
# Codegen & Reflection (typical usage)

```cpp

class Y: private X
{
public:
    int m3;

protected:
    int m4;

private:
    int m5;

public:
    int f() const;

private:
    // Some ugly macro
    BOOST_DESCRIBE_CLASS(Y, (X), (m3, f), (m4), (m5))
};
```

---
# Codegen & Reflection (modern)

What is improved:

- Type-checking
- Operating on code instead of text (unlike macros)
- Readable error messages

What can you use:

- magic_enum (reflection for enums)
- boost.pfr (reflection for aggregates)


</textarea>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script> var s = remark.create({ ratio: '16:9', highlightLines: true, highlightStyle: "tomorrow-night"}); </script></body></html>
